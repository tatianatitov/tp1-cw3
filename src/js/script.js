document.addEventListener("DOMContentLoaded", function(event) {

    //Un commentaire

    document.getElementById("connexion-lien").addEventListener("click", function(event){

        event.preventDefault();

        document.querySelector("#connexion-modale").classList.add("open");

        document.getElementById("fermer-connexion-modale").addEventListener("click", function(event){

            event.preventDefault();

            document.querySelector("#connexion-modale").classList.remove("open");

        });

    });

    var intervalId = 0;
    var $boutonScroll = document.querySelector('#retour');

    function verifieHaut() {

        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function monte() {
        intervalId = setInterval(verifieHaut, 20);
    }

    $boutonScroll.addEventListener('click', monte);


});